<!DOCTYPE HTML>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous">
    </script>
    <script src="js/handlerForm.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
        </div>
        <div class="col-sm">
            <form id="registrationForm" style="display: block">
                <h2>Регистрация</h2>
                <p id="info" class="text-danger"></p>
                <div class="form-group">
                    <label>Login</label>
                    <input type="text" name="login" class="form-control">
                    <small id="login" class="form-text text-danger"></small>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control">
                    <small id="password" class="form-text text-danger"></small>
                </div>
                <div class="form-group">
                    <label>Confirm password</label>
                    <input type="password" name="confirmPassword" class="form-control">
                    <small id="confirmPassword" class="form-text text-danger"></small>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control">
                    <small id="email" class="form-text text-danger"></small>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control">
                    <small id="name" class="form-text text-danger"></small>
                </div>
                <input type="button" value="Регистрация" id="sendRegForm" class="btn btn-primary">
            </form>
        </div>
        <div class="col-sm">
        </div>
    </div>
</div>

</body>
</html>